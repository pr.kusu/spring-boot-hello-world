#!/bin/bash
echo "Starting Job" && java $1 -jar spring-boot-hello-world.jar &
x=1
while [[ ${x} -le 1 ]]
do
  echo "Sleeping for $2 Secs"
  sleep $2
  R=$(curl -k http://localhost:8443/job-status?name=TestJob)
  if [[ ${R} = 'JOB_COMPLETE' ]]
  then
    echo "Job Finished"
    x=2
  else
    echo "Job Still running..."
  fi
done
echo "Job Complete" && exit 0