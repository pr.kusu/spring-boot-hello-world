# for testing you can create docker image and then push to google container registry for deployment
# Step 1 - run mvn clean install
# Step 2 - docker build  -t gcr.io/pcln-pl-gcr-poc/htlgenloader:<<version>> --build-arg VERSION=<<version>> -f LocalDockerFile .
# Step 3 - push image : docker push gcr.io/pcln-pl-gcr-poc/htlgenloader:<<version>>
FROM gcr.io/pcln-pl-gcr-prod/pcln-zulu11

ARG APPLICATION_NAME=spring-boot-hello-world
ARG VERSION

ENV APPLICATION_NAME=${APPLICATION_NAME}
ENV VERSION=${VERSION}

RUN test -n "${APPLICATION_NAME}" || (echo "APPLICATION_NAME  not set" && false)

RUN test -n "${VERSION}" || (echo "VERSION  not set" && false)

WORKDIR /apps/home/eng/bin

RUN mkdir -p /apps/home/eng/bin

COPY target/spring-boot-hello-world-${VERSION}.jar /apps/home/eng/bin/spring-boot-hello-world.jar
COPY start.sh /apps/home/eng/bin/start.sh
ENV JAVA_OPTIONS="-Dpcln.app.id=spring-boot-hello-world -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=7778"
#CMD java ${JAVA_OPTIONS} -Dpcln.app.id="spring-boot-hello-world"  -jar /apps/home/eng/bin/spring-boot-hello-world.jar
CMD /bin/sh /apps/home/eng/bin/start.sh ${JAVA_OPTIONS}