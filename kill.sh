#!/bin/bash
echo "Starting Job"
java -jar spring-boot-hello-world.jar &
echo "Sleeping..."
sleep 10
processes=$(ps aux | grep spring-boot-hello-world.jar -i | awk -F ' ' '{print $2}' | xargs)
echo "Processes: "$processes
for i in $processes; do kill $i; done
echo "Job Complete"
exit 0