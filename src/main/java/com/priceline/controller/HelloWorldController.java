package com.priceline.controller;

import com.priceline.JobTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    private final JobTest jobTest;
    private static final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);

    public HelloWorldController(JobTest jobTest) {
        this.jobTest = jobTest;
    }

    @GetMapping("/")
    public String hello() {
        return "Hello World!!";
    }

    @GetMapping("/start-jobs")
    public String startJobs(@RequestParam("name") String jobName) {
        return jobTest.runJob(jobName);
    }

    @GetMapping("/job-status")
    public String jobStatus(@RequestParam("name") String jobName) {
        logger.info("Checking job status...{}", jobName);
        return jobTest.jobStatus(jobName);
    }
}
