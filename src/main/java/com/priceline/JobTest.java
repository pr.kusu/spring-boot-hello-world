package com.priceline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class JobTest {

    private static final Logger logger = LoggerFactory.getLogger(JobTest.class);
    private Map<String, Long> jobStatus = new ConcurrentHashMap<>();

    @PostConstruct
    public void start(){
        logger.info("Triggering job....");
        runJob("TestJob");
    }

    public String runJob(String job) {
        logger.info("Starting Job:{} ", job);
        if (jobStatus(job).equalsIgnoreCase("JOB_RUNNING")) {
            return "JOB_ALREADY_RUNNING";
        } else {
            jobStatus.put(job, System.currentTimeMillis());
            return "JOB_STARTED";
        }

    }

    public String jobStatus(String job) {
        if (jobStatus.containsKey(job)) {
            long diff = System.currentTimeMillis() - jobStatus.get(job);
            if (Duration.ofMillis(diff).toMinutes() == 2) {
                logger.info("2 Minutes passed since last run");
                return "JOB_COMPLETE";
            }
            logger.info("Job Still running...");
            return "JOB_RUNNING";
        } else{
            logger.info("Job Not started...");
            return "NOT_STARTED";
        }
    }
}
