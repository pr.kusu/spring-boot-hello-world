#change to your home directory path
#put pclncerts on config dir or change volume path
#change the home dir on local.env file too for pclncerts
#run the project using this script and pass version parameter after you docker build
# eg. ./local-dockerrun.sh 0.0.3-SNAPSHOT

#change to your home dir
HOME_DIR=kadhikari
VERSION=0.0.5-SNAPSHOT
APP_NAME=spring-boot-hello-world
DOCKER_REGISTRY=192.168.1.13:5000

docker build -t $DOCKER_REGISTRY/$APP_NAME:$VERSION --build-arg VERSION=$VERSION .
docker push $DOCKER_REGISTRY/$APP_NAME:$VERSION
docker pull $DOCKER_REGISTRY/$APP_NAME:$VERSION

docker run  -d -v /Users/$HOME_DIR/config:/Users/$HOME_DIR/config -v /Users/$HOME_DIR/apps/home/eng/logs/$APP_NAME:/apps/home/eng/logs -p 8443:8443 -p 7778:7778 $DOCKER_REGISTRY/$APP_NAME:$VERSION